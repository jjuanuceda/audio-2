﻿using UnityEngine;

public class COMPLETE_HornAudioController : MonoBehaviour
{
    //get references to the AudioSource and AudioClip in the Inspector 
    public AudioSource audioSource;
    public AudioClip hornAudioclip;

    //theOnTriggerEnter is a MonoBehavior method that is called when two colliders (one marked trigger) intersest
    private void OnTriggerEnter(Collider other)
    {
        //check if the object that is being collided with has the "Player" tag
        if (other.transform.CompareTag("Player"))
        {
            //play the horn sound
            audioSource.PlayOneShot(hornAudioclip);
        }
    }
}
